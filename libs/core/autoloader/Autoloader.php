<?php
namespace core\autoloader;

class AutoloaderException extends \Exception {}

class Autoloader
{
    protected $config;

    public function __construct()
    {
        $this->config = json_decode(
            file_get_contents('../config/autoloader/autoloader.json'),
            true
        );

        spl_autoload_register(
            array(
                $this,
                'load'
            )
        );
    }

    public static function init() : void
    {
        $autoloader = new static();
    }

    protected function load(string $class) : void
    {
        $namespace = (@$this->config[$class] ?: $class);

        $path = str_replace('\\', '/', $namespace).'.php';

        include_once '../libs/'.$path;

        if ($namespace != $class) {
            class_alias($namespace, $class);
        }
    }
}