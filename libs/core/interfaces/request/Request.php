<?php
namespace core\interfaces\request;

use \core\interfaces\url\Url;

interface Request
{
    public function getUrl() : Url;

    public function get(string $name) : ?string;
}