<?php
namespace core\interfaces\url;

interface Url
{
    public function getCurrent() : string;
}