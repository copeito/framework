<?php
namespace core\app;

use \Request;

class App
{
    protected static self $instance;

    public static function getInstance() : self
    {
        if (!isset(static::$instance)){
            static::$instance = new static();
        }

        return self::$instance;
    }

    public function run() : void 
    {
        $request = new Request;
        print "App:run ".$request->get('id')."<br>";
    }
}