<?php
namespace vendor\copeito\url;

class Url implements \core\interfaces\url\Url
{
    protected readonly array $data;
    protected readonly string $url;

    public function __construct(string $url = null)
    {
        $this->url = $url ?: $this->getCurrent();

        $this->data = parse_url($this->url);
    }

    public function getCurrent(): string
    {
        return (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }
}
