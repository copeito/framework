<?php
namespace vendor\copeito\request;

use vendor\copeito\url\Url;

class Request implements \core\interfaces\request\Request
{
    protected readonly Url $url;

    public function __construct()
    {
        $this->url = new Url();
    }

    public function getUrl(): Url
    {
        return $this->url;
    }

    public function get(string $name): ?string
    {
        return (@$_REQUEST[$name] ?: null);
    }
}