<?php
namespace core;

include_once '../libs/core/autoloader/Autoloader.php';

autoloader\Autoloader::init();

app\App::getInstance()->run();